package com.example.m09_apiproject.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.m09_apiproject.R
import com.example.m09_apiproject.databinding.FragmentWatchDetailsBinding
import com.example.m09_apiproject.viewmodel.WatchesViewModel

class WatchDetailsFragment : Fragment() {
    lateinit var binding: FragmentWatchDetailsBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentWatchDetailsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewModel = ViewModelProvider(requireActivity())[WatchesViewModel::class.java]
        val id = arguments?.getInt("id")

        val watch = viewModel.currentWatch.value!!
//        viewModel.checkIfCharacterIsInFavorites(character)

//        viewModel.isCharacterInFavorites.observe(viewLifecycleOwner) {
//            if (it) { binding.saveImageButton.setImageResource(R.drawable.ic_baseline_bookmark_24_v2) }
//            else { binding.saveImageButton.setImageResource(R.drawable.ic_baseline_bookmark_24) }
//        }

        Glide.with(requireContext())
            .load(watch.imageUrl)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .into(binding.watchImage)
        binding.brandTextView.text = watch.brand
        binding.modelTextView.text = watch.model
        binding.priceTextView.text = "${watch.price}€"


//        binding.saveImageButton.setOnClickListener {
//            val char = CharactersEntity(character.id, character.fullName, character.title, character.family, true, character.imageUrl)
//            if (viewModel.isCharacterInFavorites.value!!) {
//                deleteCharacter(char)
//                viewModel.isCharacterInFavorites.postValue(false)
//            }
//            else {
//                insertCharacter(char)
//                viewModel.isCharacterInFavorites.postValue(true)
//            }
//        }

    }

}