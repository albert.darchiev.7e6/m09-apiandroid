package com.example.m09_apiproject.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.m09_apiproject.R
import com.example.m09_apiproject.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}