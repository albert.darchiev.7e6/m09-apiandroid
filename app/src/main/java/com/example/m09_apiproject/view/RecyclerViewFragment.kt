package com.example.m09_apiproject.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.m09_apiproject.R
import com.example.m09_apiproject.databinding.FragmentRecyclerViewBinding
import com.example.m09_apiproject.model.WatchItem
import com.example.m09_apiproject.model.WatchesAdapter
import com.example.m09_apiproject.utils.OnClickListener
import com.example.m09_apiproject.viewmodel.WatchesViewModel

@SuppressLint("StaticFieldLeak")
lateinit var watchesAdapter: WatchesAdapter
lateinit var linearLayoutManager: RecyclerView.LayoutManager
lateinit var binding: FragmentRecyclerViewBinding
lateinit var quoteViewModel: WatchesViewModel

class RecyclerViewFragment : Fragment(), OnClickListener {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentRecyclerViewBinding.inflate(inflater,container,false)
        return binding.root
    }
    private fun getWatch(): MutableList<WatchItem>{
        val watches = mutableListOf<WatchItem>()
        watches.add(WatchItem(11,"AAAA","33XCE", 33, "https://www.shutterstock.com/image-vector/url-shortener-man-pushes-address-600w-2201694049.jpg", "MALE", "asd"))
        return watches
    }
    fun setupRecyclerView(list: List<WatchItem>){
        watchesAdapter = WatchesAdapter(list, this)
        val myAdapter = WatchesAdapter(list, this)
        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimization el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = myAdapter
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        watchesAdapter = WatchesAdapter(getWatch(), this)
        linearLayoutManager = GridLayoutManager(context, 2) //3 columnes
        setupRecyclerView(getWatch())

        binding.addNewButton.setOnClickListener {
            findNavController().navigate(R.id.action_recyclerViewFragment_to_addWatchFragment)
        }

        val viewModel = ViewModelProvider(requireActivity())[WatchesViewModel::class.java]

        binding.switch1.setOnClickListener {
            viewModel.fetchData(binding.switch1.isActivated)
        }

        viewModel.data.observe(viewLifecycleOwner){
            setupRecyclerView(it)
        }

    }

    override fun onClick(watch: WatchItem) {
        val action = RecyclerViewFragmentDirections.actionRecyclerViewFragmentToWatchDetailsFragment2(watch.id)
        val viewModel = ViewModelProvider(requireActivity())[WatchesViewModel::class.java]
        viewModel.currentWatch.postValue(watch)

        findNavController().navigate(action)
    }
}