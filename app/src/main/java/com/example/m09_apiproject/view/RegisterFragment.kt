package com.example.m09_apiproject.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.m09_apiproject.R
import com.example.m09_apiproject.api.Repository
import com.example.m09_apiproject.databinding.FragmentRegisterBinding
import com.example.m09_apiproject.model.User
import com.example.m09_apiproject.viewmodel.WatchesViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class RegisterFragment : Fragment() {
    lateinit var binding: FragmentRegisterBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRegisterBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.goToLogin.setOnClickListener {
            findNavController().navigate(R.id.loginFragment)
        }

        binding.registerButton.setOnClickListener {
            val pass1 = binding.regPassword.text.toString()
            val pass2 = binding.regPassword2.text.toString()
            val usern = binding.regUsername.text.toString()

            if (pass1.isEmpty() || pass2.isEmpty() || usern.isEmpty()) Toast.makeText(
                context,
                "Completa todos los campos",
                Toast.LENGTH_SHORT
            ).show()
            else if (pass1 != pass2) Toast.makeText(
                context,
                "Las contraseñas no son iguales",
                Toast.LENGTH_SHORT
            ).show()
            else registerUser(User(usern, pass1))
        }
    }

fun registerUser(user: User){
    CoroutineScope(Dispatchers.IO).launch {
        val viewModel = ViewModelProvider(requireActivity())[WatchesViewModel::class.java]
        viewModel.repository = Repository(user.username, user.password)
        val repository = Repository(user.username, user.password)
        val response = repository.register(User(user.username, user.password))
        withContext(Dispatchers.Main){
            if (response.isSuccessful){
                Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show()
                findNavController().navigate(R.id.loginFragment)
            }
            else{
                Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show()

            }
        }
    }
}
}
