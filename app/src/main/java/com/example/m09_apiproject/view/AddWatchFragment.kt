package com.example.m09_apiproject.view

import android.os.Binder
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.m09_apiproject.R
import com.example.m09_apiproject.api.Repository
import com.example.m09_apiproject.databinding.FragmentAddWatchBinding
import com.example.m09_apiproject.model.User
import com.example.m09_apiproject.model.WatchItem
import com.example.m09_apiproject.viewmodel.WatchesViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class AddWatchFragment : Fragment() {
    lateinit var binding: FragmentAddWatchBinding
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentAddWatchBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewModel = ViewModelProvider(requireActivity())[WatchesViewModel::class.java]

        binding.saveButton.setOnClickListener {
            val genderSpinner = binding.genderSpinner
            val selectedPosition = genderSpinner.selectedItemPosition

            val brand = binding.brandEt.text.toString()
            val model = binding.modelEt.text.toString()
            val price = binding.priceEt.text.toString().toInt()
            val gender = genderSpinner.getItemAtPosition(selectedPosition).toString()

            addWatch(WatchItem(1, brand, model, price, "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQGm4VFj_glmzl-VshqzRXv-Un0MR42TVL-Xg&usqp=CAU", gender, viewModel.currentUser.username))
//            findNavController().navigate(R.id.action_addWatchFragment_to_recyclerViewFragment)
        }


    }
    fun addWatch(watch: WatchItem){
        try {
            val viewModel = ViewModelProvider(requireActivity())[WatchesViewModel::class.java]
            val user = viewModel.currentUser
            CoroutineScope(Dispatchers.IO).launch {
                viewModel.repository = Repository(user.username, user.password)
                val repository = Repository(user.username, user.password)
                val response = repository.addWatch(watch)
                withContext(Dispatchers.Main){
                    if (response.isSuccessful){
                        Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show()
                        findNavController().navigate(R.id.action_addWatchFragment_to_recyclerViewFragment)
                    }
                    else{
                        Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }catch (e:Exception){
            println(e)
        }

    }

}