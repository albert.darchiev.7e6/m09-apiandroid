package com.example.m09_apiproject.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.m09_apiproject.R
import com.example.m09_apiproject.api.Repository
import com.example.m09_apiproject.databinding.FragmentLoginBinding
import com.example.m09_apiproject.model.User
import com.example.m09_apiproject.viewmodel.WatchesViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class LoginFragment : Fragment() {
    lateinit var binding: FragmentLoginBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentLoginBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnIrCrearCuenta.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }

        binding.loginButton.setOnClickListener {
            val user = binding.etUser.text.toString()
            val pass = binding.etPassword.text.toString()
            loginUser(User(user, pass))

        }
    }

    fun loginUser(user: User){
        CoroutineScope(Dispatchers.IO).launch {
            val viewModel = ViewModelProvider(requireActivity())[WatchesViewModel::class.java]
            viewModel.repository = Repository(user.username, user.password)
            val repository = Repository(user.username, user.password)
            val response = repository.loginUser(User(user.username, user.password))
            withContext(Dispatchers.Main){
                if (response.isSuccessful){
                    viewModel.currentUser = user
                    Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show()
                    findNavController().navigate(R.id.action_loginFragment_to_recyclerViewFragment2)
                }
                else{
                    Toast.makeText(context, response.message(), Toast.LENGTH_SHORT).show()

                }
            }
        }
    }

}

