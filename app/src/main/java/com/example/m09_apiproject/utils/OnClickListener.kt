package com.example.m09_apiproject.utils

import com.example.m09_apiproject.model.WatchItem

interface OnClickListener {
    fun onClick(watch: WatchItem)
}