package com.example.m09_apiproject.api

import com.example.m09_apiproject.model.User
import com.example.m09_apiproject.model.WatchItem

class Repository(val username: String,val  password:String) {
    val apiInterface = WatchInterface.create(username, password)
    suspend fun getWatches() = apiInterface.getAllWatches()
    suspend fun getWatchesById(id:Int) = apiInterface.getWatchById(id)
    suspend fun getMyWatches() = apiInterface.getMyWatches()
    suspend fun addWatch(watchItem: WatchItem) = apiInterface.addWatch(watchItem)

    suspend fun loginUser(user: User) = apiInterface.login(user)
    suspend fun register(user:User) = apiInterface.register(user)
}