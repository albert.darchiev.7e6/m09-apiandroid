package com.example.m09_apiproject.api

import com.example.m09_apiproject.model.User
import com.example.m09_apiproject.model.WatchItem
import com.example.m09_apiproject.model.Watches
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.ResponseBody

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
interface WatchInterface {
    @GET("/watch")
    suspend fun getAllWatches(): Response<Watches>
    @GET("/watch/{id}")
    suspend fun getWatchById(@Path("id") id: Int): Response<WatchItem>
    @GET("/watch/my-watches")
    suspend fun getMyWatches(): Response<Watches>
    @POST("/watch")
    suspend fun addWatch(@Body watch: WatchItem): Response<ResponseBody>
    @POST("/login")
    suspend fun login(@Body user: User): Response<ResponseBody>
    @POST("/register")
    suspend fun register(@Body user: User): Response<ResponseBody>


    companion object {
        val BASE_URL = "http://192.168.1.78:8080"
        fun create(username: String, password: String): WatchInterface {
            val digestAuthenticator = DigestAuthenticator(Credentials(username, password))
            val client = OkHttpClient.Builder().authenticator(digestAuthenticator).build()
            val gson = GsonBuilder().setLenient().create()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(WatchInterface::class.java)
        }
    }
}