package com.example.m09_apiproject.model

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.example.m09_apiproject.R
import com.example.m09_apiproject.databinding.ItemWatchBinding
import com.example.m09_apiproject.utils.OnClickListener

class WatchesAdapter(private val watches: List<WatchItem>, private val listener: OnClickListener):
    RecyclerView.Adapter<WatchesAdapter.ViewHolder>() {
    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemWatchBinding.bind(view)
        fun setListener(watch: WatchItem){
            binding.root.setOnClickListener {
                listener.onClick(watch)
            }
        }
    }
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_watch, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return watches.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val watch = watches[position]
        with(holder){
            setListener(watch)
            binding.idTextView.text = watch.id.toString()
            binding.modelTextView.text = watch.model
            binding.brandTextView.text = watch.brand
            binding.priceTextView.text = "${watch.price}€"
            Glide.with(context)
                .load(watch.imageUrl)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .transform(RoundedCorners(15))
                .into(binding.imageView)
        }
    }
}