package com.example.m09_apiproject.model

data class WatchItem(
    val id: Int,
    val brand: String,
    val model: String,
    val price: Int,
    val imageUrl: String,
    val gender: String,
    val seller: String
)