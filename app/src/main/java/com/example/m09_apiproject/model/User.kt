package com.example.m09_apiproject.model

data class User(val username: String, val password: String)