package com.example.m09_apiproject.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.m09_apiproject.api.Repository
import com.example.m09_apiproject.model.User
import com.example.m09_apiproject.model.WatchItem
import com.example.m09_apiproject.model.Watches
import kotlinx.coroutines.*
import retrofit2.Response

class WatchesViewModel: ViewModel() {
    var currentUser = User("123", "123")
    lateinit var repository: Repository
    var data = MutableLiveData<Watches>()
    var isWatchInFavourite = MutableLiveData<Boolean>()
    var currentWatch = MutableLiveData<WatchItem>()

    lateinit var response: Response<Watches>


    fun fetchData(justMine: Boolean){
        CoroutineScope(Dispatchers.IO).launch {
            if (justMine) response = repository.getMyWatches()
            else response = repository.getWatches()

            withContext(Dispatchers.Main) {
                if(response.isSuccessful){
                    data.postValue(response.body())
                }
                else{
                    Log.e("Error :", response.message())
                }
            }
        }
    }

    fun fetchWatchesById(id: Int){
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getWatchesById(id)
            withContext(Dispatchers.Main) {
                if(response.isSuccessful){
                    val watch = Watches()
                    watch.add(response.body() as WatchItem)
                    data.postValue(watch)
                }
                else{
                    Log.e("Error :", response.message())
                }
            }
        }
    }

//    fun fetchFavoriteCharacterById(id: Int) {
//        CoroutineScope(Dispatchers.IO).launch {
//            data.postValue(CharacterApplication.database.characterDao().getCharacterById(id).map { CharactersEntity -> CharactersItem(CharactersEntity.family, "", CharactersEntity.fullname, CharactersEntity.id, "", CharactersEntity.imageUrl, "", CharactersEntity.title) }.toCollection(CharactersX()))
//        }
//    }

//    fun fetchFavoriteData() {
//        CoroutineScope(Dispatchers.IO).launch {
//            data.postValue(CharacterApplication.database.characterDao().getAllCharacters().map { CharactersEntity -> CharactersItem(CharactersEntity.family, "", CharactersEntity.fullname, CharactersEntity.id, "", CharactersEntity.imageUrl, "", CharactersEntity.title) }.toCollection(CharactersX()))
//        }
//    }

    fun characterData(id: Int): WatchItem?{
        var i = 0
        var item:WatchItem? = null
        while (i in data.value!!.indices&&item==null){
            if(id == data.value!![i].id) item = data.value!![i]
            i++
        }
        return item
    }

//    fun checkIfCharacterIsInFavorites(character: CharactersItem) {
//        runBlocking {
//            withContext(Dispatchers.IO) {
//                val favCharacter = CharacterApplication.database.characterDao().getCharacterById(character.id)
//                isCharacterInFavorites.postValue(favCharacter.size != 0)
//            }
//        }
//    }
}
